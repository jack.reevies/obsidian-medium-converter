declare global {
  namespace NodeJS {
    interface ProcessEnv {
      FILE: string
      VAULT: string
      MEDIUM_TOKEN: string
      MEDIUM_USER: string
    }
  }
}

export { }
