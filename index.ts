import highlight from 'highlight.js'
import { parse } from 'marked'
import FormData from 'form-data'
//@ts-ignore
const unescape = require('unescape')
import fetch from 'node-fetch'

import dotenv from 'dotenv'
import { createReadStream, readFileSync } from 'fs'
import { getFiles } from './helpers'
import { sep } from 'path'
dotenv.config()

/**
 * Replaces code blocks in an html string with that of medium formatted markup (to get syntax highlighting on a medium post)
 * @param html raw html of the blog post
 * @returns modified html with medium formatted code blocks
 */
function replaceCodeBlocks(html: string) {
  const blocks = html.matchAll(/<pre><code class="language-(.+)">((.|\n)+?)<\/code><\/pre>/gm)
  for (const block of blocks) {
    const [_, lang, code] = block
    const sanitized = unescape(code)
    const highlighted = highlight.highlight(sanitized, { language: lang }).value.replace(/\n/g, '<br>')
    const mediumWrap = `<pre data-code-block-mode="2" spellcheck="false" data-code-block-lang="${lang}">${highlighted}</pre>`
    html = html.replace(_, mediumWrap)
  }

  return html
}

/**
 * Generates html markup for a medium formatted image (figure with caption)
 * @param img slug or path to the image
 * @param caption caption of the image
 * @returns medium formatted image markup
 */
function formatFigure(img: string, caption: string) {
  if (img.startsWith('http')) return `<figure><img src="${img}"><figcaption>${caption}</figcaption></figure>`

  return `<figure><img src="https://miro.medium.com/v2/resize:fit:2000/${img}"><figcaption>${caption}</figcaption></figure>`
}

/**
 * Replaces standard image embeds in an html string with that of medium formatted markup
 * @param html raw html of the blog post
 * @returns modified html with medium formatted images
 */
function replaceExternalImages(html: string) {
  const images = html.matchAll(/<img src="(.+?)" alt="(.*)"[^>]*?>/g)
  for (const image of images) {
    const [_, src, alt] = image
    html = html.replace(_, formatFigure(src, alt))
  }

  return html
}

/**
 * Uploads images to medium and replaces the markdown with the medium image url
 * @param html raw html of the blog post
 * @returns modified html with medium image urls
 */
async function uploadInternalImages(html: string) {
  const files = await getFiles(process.env.VAULT)
  const images = html.matchAll(/(?:!\[\[([^|]+)(?:\|?(.*?))\]\])/gm)
  const imagePaths: Record<string, { img: string, caption: string, replace: string }> = {}

  for (const image of images) {
    const [_, path, caption] = image
    const foundFile = files.find((file: string) => file.endsWith(path))
    if (foundFile) {
      imagePaths[path] = { img: foundFile, caption: caption || '', replace: _ }
    }
  }

  for (const path in imagePaths) {
    const file = imagePaths[path]
    const url = await uploadImageToMedium(file.img)
    const slug = url.split('/').pop() || ''
    console.log(`Uploaded ${file.img} as ${url}`)
    html = html.replace(file.replace, formatFigure(slug, file.caption))
  }

  return html
}

/**
 * Uploads an image to medium
 * @param file path to the image
 * @returns url of the uploaded image
 */
async function uploadImageToMedium(file: string) {
  const formData = new FormData()
  const fileStream = createReadStream(file)
  formData.append('image', fileStream, file)
  const res = await fetch('https://api.medium.com/v1/images', {
    method: 'POST',
    body: formData,
    headers: {
      Authorization: `Bearer ${process.env.MEDIUM_TOKEN}`,
      'User-Agent': 'PostmanRuntime/7.26.8',
    },
  })
  const text = await res.text()
  try {
    const json = JSON.parse(text)
    return json.data.url
  } catch (e) {
    console.error(e)
  }
}

/**
 * Uploads the blog post to medium
 * @param body html of the blog post
 */
async function uploadToMedium(title: string, body: string) {
  const res = await fetch(`https://api.medium.com/v1/users/${process.env.MEDIUM_USER}/posts`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${process.env.MEDIUM_TOKEN}`,
      'Content-Type': 'application/json',
      'User-Agent': 'PostmanRuntime/7.26.8',
    },
    body: JSON.stringify({
      title: title || `Untitled post ${new Date().toLocaleString()}`,
      contentFormat: 'html',
      content: body,
      publishStatus: 'draft',
      tags: [],
      canonicalUrl: undefined,
      license: undefined,
      notifyFollowers: false,
    })
  })

  const json = await res.json()
  return json
}

async function start() {
  // Read the markdown source and replace non-breaking spaces with regular spaces
  const contents = readFileSync(`${process.env.VAULT}${sep}${process.env.FILE}`).toString().replace(/\u00a0/g, ' ')
  const title = process.env.FILE.split(sep).pop()!.split('.')[0]

  // Parse the markdown to html
  const html = parse(contents)

  // Replace code blocks with medium formatted code blocks
  const htmlWithCodeBlocks = replaceCodeBlocks(html)

  // Replace standard image embeds with medium formatted markup
  const htmlWithExternalImgs = replaceExternalImages(htmlWithCodeBlocks)

  // Upload images to medium and replace the untouched markdown with the medium image url
  const htmlWithFigureImgs = await uploadInternalImages(htmlWithExternalImgs)

  let edited = htmlWithFigureImgs
  const h1s = htmlWithFigureImgs.matchAll(/<h1>(.+?)<\/h1>/g)
  let i = 0
  for (const h1 of h1s) {
    const [_, title] = h1
    edited = edited.replace(_, `<h1 id="${++i}">${title}</h1>`)
  }

  // Upload HTML version of the blog post to medium
  const json = await uploadToMedium(title, edited)

  // TODO: Anchor links, setting description, other metadata

  console.log(`Uploaded ${title} to medium: ${json.data.url}`)
}

start()