import { Dirent, existsSync } from "fs"
import { readdir } from "fs/promises"
import { resolve } from "path"

/**
 * Recursively reads files in a directory. Returns as an array of files relative to the start dir
 */
export async function getFiles(dir: string): Promise<string[]> {
  if (!existsSync(dir)) {
    return []
  }
  const dirents = await readdir(dir, { withFileTypes: true })
  const files: unknown[] = await Promise.all(dirents.map((dirent: Dirent) => {
    const res = resolve(dir, dirent.name)
    return dirent.isDirectory() ? getFiles(res) : res
  }))
  return Array.prototype.concat(...files)
}